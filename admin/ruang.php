  <?php
 include "header.php";
 ?>

            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li> 
                                <li class="active">Data Ruang</li>
                            </ol>
                        </div>
                    </div>
                    <ul class="nav nav-pills nav-pills-rose">
                        <li class="nav-item tab-all"><a href="input_ruang.php" class="nav-link active show">Tambah Ruang</a></li>
                         
                    </ul>
                    <div class="tab-content tab-space">
                       <div class="tab-pane active show" id="tab1">
                         <div class="row">
                              <div class="col-sm-12">
                                <div class="card-box">
                                  <div class="card-head">
                                    <header>Data Ruang</header> 
                                 </div>
                                      <div class="table-scrollable">
                                        <table class="table table-hover table-checkable order-column full-width" id="example4">
                                            <thead>
                                                <tr>
                                                    <th class="column-title">No </th> 
                                                    <th class="column-title">Nama Ruangan  </th>
                                                    <th class="column-title">Kode Ruangan </th>
                                                    <th class="column-title">Keterangan </th>  
                                                    <th class="column-title no-link last"><span class="nobr">Action</span>
                                                    </th>                  
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                <?php
                                                include "../koneksi.php";
                                                $no=1;
                                                $select=mysqli_query($koneksi,"select * from ruang order by id_ruang desc");
                                                while($data=mysqli_fetch_array($select))
                                                {
                                                ?>
                                                <tr>
                                                    <td><?php echo $no++; ?></td> 
                                                    <td><?php echo $data['nama_ruang']; ?></td>
                                                    <td><?php echo $data['kode_ruang']; ?></td>
                                                    <td><?php echo $data['keterangan']; ?></td> 
                                                    <td> 
                                                    <a href="edit_ruang.php?id_ruang=<?php echo $data['id_ruang']; ?>" class="btn btn-tbl-edit btn-xs"><i class="fa fa-pencil"></i></a>
                                                    <a href="hapus_ruang.php?id_ruang=<?php echo $data['id_ruang']; ?>" class="btn btn-tbl-delete btn-xs"><i class="fa fa-trash-o "></i></a></td>    
                                                </tr>
                                                <?php
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                      </div>
                                    </div>
                                </div>
                              </div>
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
            <!-- end page content -->
    
 <?php
    include'footer.php'; ?>
 