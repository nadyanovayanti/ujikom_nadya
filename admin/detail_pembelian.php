<?php
 include "header.php";  
?>

            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                      <div class="row">
                        <!-- start widget -->
          <div class="state-overview">
            <div class="row">
                  <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-blue">
                      <span class="info-box-icon push-bottom"><i class="material-icons">style</i></span>
                      <div class="info-box-content">
                      <?php 
                          include "../koneksi.php";
                          $dt = mysqli_query($koneksi, "SELECT sum(jumlah)as jumlah From inventaris where kondisi='Baik'");
                  $d = mysqli_fetch_array($dt);
                        ?>
                        <span class="info-box-text">Barang Masuk</span>
                        <span class="info-box-number"><?php echo $d['jumlah'];?></span>
                         
                            
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-orange">
                      <span class="info-box-icon push-bottom"><i class="material-icons">card_travel</i></span>
                      <div class="info-box-content">
                         <?php 
                          include "../koneksi.php";
                          $dtd = mysqli_query($koneksi, "SELECT sum(jumlah)as jumlah From inventaris where kondisi='Rusak'");
                  $dd = mysqli_fetch_array($dtd);
                  $r= $dd['jumlah'];
                  $y=0;
                        ?>
                        <span class="info-box-text">Barang Rusak</span> 
                           <span class='info-box-number'><?php
                  if($r<1){
                    echo "$y";
                  }else{
                    echo "$r";
                  }?>
                        </span>
                         
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-purple">
                      <span class="info-box-icon push-bottom"><i class="material-icons">phone_in_talk</i></span>
                      <div class="info-box-content">
                       <span class="info-box-text">Peminjaman</span>
                         <?php 
                          include "../koneksi.php";
                          $dtdd = mysqli_query($koneksi, "SELECT sum(status_peminjaman)as status_peminjaman From detail_pinjam where status_peminjaman='Pinjam'");
                          $ddd = mysqli_fetch_array($dtdd);
                          $t=$ddd['status_peminjaman'];
                          $k=0;
                  ?>
                  <span class='info-box-number'><?php
                  if($t>=1){
                    echo "$t";
                  }else{
                    echo "$k";
                  }
                        ?>           
                        </span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-success">
                      <span class="info-box-icon push-bottom"><i class="material-icons">monetization_on</i></span>
                      <div class="info-box-content"> 
                        <span class="info-box-number"><?php    
                            date_default_timezone_set("asia/Bangkok");           
                  $jam=date("d-m-Y H.i.s"); 
                  echo "$jam";
                  ?></span>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                </div>
            </div>
          </div>
          <!-- end widget --> 

                <div class="col-md-12">
                                    <div class="card card-topline-purple">
                                        <div class="card-head">
                                          <a href="dta_pinjam.php" class="btn">Close</a>
                                            <header>Data Transaksi</header>
                                            <div class="tools">
                                                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                                <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                                <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                            </div>
                                        </div>
                                        <div class="card-body ">
                                        <div class="table-responsive">
                                          <table id="example2" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Peminjam</th>
                                                        <th>Jumlah</th>
                                                        <th>Status</th>
                                                        <th>ID</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody> 
                                                 <?php // Load file koneksi.php
  include "../koneksi.php";
  $id_peminjaman = $_GET ['id_peminjaman'];
  $query = "SELECT * FROM detail_pinjam JOIN inventaris on inventaris.id_inventaris = detail_pinjam.id_inventaris where detail_pinjam.id_peminjaman='$id_peminjaman' ";
  $sql = mysqli_query($koneksi, $query); // Eksekusi/Jalankan query dari variabel $query
  $no=1;
  while($data = mysqli_fetch_array($sql)){
?>
          <tr>
            <td><?php echo $no++; ?></td>
            <td><?php echo $data['nama']; ?></td>
            <td><?php echo $data['jumlah']; ?></td>
            <td><?php echo $data['status_peminjaman']; ?></td>
            <td><?php echo $data['id_peminjaman']; ?></td>
            <td>
            <?php if($data['status_peminjaman'] == 'Pinjam') { ?>
            <form action="proses_pengembalian.php" method="post">
                  <input type="hidden" name="id_peminjaman" value="<?php echo $data['id_peminjaman']?>">
                  <button type="submit">Kembalikan</button>
            </form>
            <?php }else{ ?>
            <input type="hidden" name="id_peminjaman" value="<?php echo $data['id_peminjaman']?>">
                  <h9>Kembali</h9>
            <?php } ?> 
            </td>
          </tr>
          <?php } ?>
                                                </tbody>
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
              </div>
            </div> 
                     
                    </div>
                </div> 
            </div>
            <!-- end page content -->
    
 <?php
    include'footer.php'; ?>
 