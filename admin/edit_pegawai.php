<?php
 include "header.php";
include "../koneksi.php";
$id_pegawai=$_GET['id_pegawai'];

$select=mysqli_query($koneksi,"select * from pegawai where id_pegawai='$id_pegawai'");
$data=mysqli_fetch_array($select);
?>
  <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                     <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li> 
                                 <li><a class="parent-item" href="pegawai.php">Data Pegawai</a>&nbsp;<i class="fa fa-angle-right"></i>
                                <li class="active"> Edit Pegawai</li>
                            </ol>
                        </div>
                    </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="card-box">
                  <div class="card-head">
                    <header>Edit Pegawai</header>                     
                  </div>
                  <form action="update_pegawai.php?id_pegawai=<?php echo $id_pegawai;?>" method="post" enctype="multipart/form-data" name="form1" id="form1" class="card-body row">
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="nama_pegawai" id="nama_pegawai" value="<?php echo $data['nama_pegawai'];?>">
                                 <label class = "mdl-textfield__label">Nama Pegawai</label>
                              </div>
                          </div>
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="nip" id="nip" value="<?php echo $data['nip'];?>">
                                 <label class = "mdl-textfield__label">Nip</label>
                              </div>
                          </div>
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="alamat" id="alamat" value="<?php echo $data['alamat'];?>">
                                 <label class = "mdl-textfield__label">Alamat</label>
                              </div>
                          </div>
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="username" id="username" value="<?php echo $data['username'];?>">
                                 <label class = "mdl-textfield__label">Username</label>
                              </div>
                          </div>
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="password" id="password" value="<?php echo $data['password'];?>">
                                 <label class = "mdl-textfield__label">Pasword</label>
                              </div>
                          </div>
                                                   
                         <div class="col-lg-12 p-t-20 text-center"> 
                          <button name="simpan" type="submit" id="simpan" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Simpan</button> 

                      <a href="dt_msk.php" type="cancel" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</a>
                          </div>
                </form>   

                      
                  </div>
                </div>
              </div>
            </div> 
                </div>
            </div>
            <!-- end page content -->
   <?php
 include "footer.php";
 ?>