<?php
 include "header.php";
include "../koneksi.php";
$id_petugas=$_GET['id_petugas'];

$select=mysqli_query($koneksi,"select * from petugas where id_petugas='$id_petugas'");
$data=mysqli_fetch_array($select);
?>
 
  <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                     <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li> 
                                 <li><a class="parent-item" href="petugas.php">Data Petugas</a>&nbsp;<i class="fa fa-angle-right"></i>
                                <li class="active"> Edit Petugas</li>
                            </ol>
                        </div>
                    </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="card-box">
                  <div class="card-head">
                    <header>Edit Petugas</header>                     
                  </div>
                  <form action="update_petugas.php?id_petugas=<?php echo $id_petugas;?>" method="post" enctype="multipart/form-data" name="form1" id="form1" class="card-body row">
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type ="text" name="nama_petugas" id="nama_petugas" value="<?php echo $data['nama_petugas'];?>">
                                 <label class = "mdl-textfield__label">Nama Petugas</label>
                              </div>
                          </div>
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "username" name="username" id="username" value="<?php echo $data['username'];?>">
                                 <label class = "mdl-textfield__label">Username</label>
                              </div>
                          </div>
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "password" name="password" id="password" value="<?php echo $data['password'];?>">
                                 <label class = "mdl-textfield__label">Password</label>
                              </div>
                          </div>
                           <?php
  include "../koneksi.php";
  $result = mysqli_query($koneksi,"select * from level order by id_level asc ");
  $jsArray = "var id_level = new Array();\n";
  ?>
 <div class="col-lg-6 p-t-20"> 
    <label>Level :</label> <select class="form-control" name="id_level" onchange="changeValue(this.value)">
  <option selected="selected"><?php echo $data['id_level'];?>
  <?php 
  while($row = mysqli_fetch_array($result)){
    echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
    $jsArray .= "id_level['". $row['id_level']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
  }
  ?>
</option>
</select>
</div>
   <div class="form-group">
      <img src="<?php echo $data['img'];?>" height="100px">
      <p>Pilih File Gambar : <br/><input name='filegbr' id='Filegambar' type='file'></p>
    </div>
                                                   
                         <div class="col-lg-12 p-t-20 text-center"> 
                          <button name="simpan" type="submit" id="simpan" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Simpan</button> 

                      <a href="petugas.php" type="cancel" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</a>
                          </div>
                </form>   

                      
                  </div>
                </div>
              </div>
            </div> 
                </div>
            </div>
            <!-- end page content -->
   <?php
 include "footer.php";
 ?>