 <?php
 include "header.php";
 ?>

            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li> 
                                <li class="active">Data Jenis Inventaris</li>
                            </ol>
                        </div>
                    </div>
                    <ul class="nav nav-pills nav-pills-rose">
                        <li class="nav-item tab-all"><a href="input_jenis.php" class="nav-link active show">Tambah Jenis Inventaris</a></li>
                         
                    </ul>
                    <div class="tab-content tab-space">
                       <div class="tab-pane active show" id="tab1">
                         <div class="row">
                              <div class="col-sm-12">
                                <div class="card-box">
                                  <div class="card-head">
                                    <header>Data Jenis Inventaris</header> 
                                 </div>
                                      <div class="table-scrollable">
                                        <table class="table table-hover table-checkable order-column full-width" id="example4">
                                            <thead>
                                                <tr>
                                                    <th class="column-title">No </th> 
                                                    <th class="column-title">Nama Jenis Inventaris </th>
                                                    <th class="column-title">Kode Jenis </th>
                                                    <th class="column-title">Keterangan </th>  
                                                    <th class="column-title no-link last"><span class="nobr">Action</span>
                                                    </th>                        
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                         <?php
            include "../koneksi.php";
            $no=1;
            $select=mysqli_query($koneksi,"select * from jenis order by id_jenis desc");
            while($data=mysqli_fetch_array($select))
            {
            ?>
            <tr>
                <td><?php echo $no++; ?></td> 
                <td><?php echo $data['nama_jenis']; ?></td>
                <td><?php echo $data['kode_jenis']; ?></td>
                <td><?php echo $data['keterangan']; ?></td> 
                <td> 
                <a href="edit_jenis.php?id_jenis=<?php echo $data['id_jenis']; ?>" class="btn btn-tbl-edit btn-xs"><i class="fa fa-pencil"></i></a></a>
                <a href="hapus_jenis.php?id_jenis=<?php echo $data['id_jenis']; ?>" class="btn btn-tbl-delete btn-xs"><i class="fa fa-trash-o "></i></a></td>    
            </tr>
            <?php
                }
            ?>
                                            </tbody>
                                        </table>
                                      </div>
                                    </div>
                                </div>
                              </div>
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
            <!-- end page content -->
    
 <?php
    include'footer.php'; ?>
 