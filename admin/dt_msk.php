  <?php
 include "header.php";
 ?>

            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li> 
                                <li class="active">Data Masuk Inventaris</li>
                            </ol>
                        </div>
                    </div>
                    <ul class="nav nav-pills nav-pills-rose">
                        <li class="nav-item tab-all"><a href="input_inventaris.php" class="nav-link active show">Tambah Inventaris</a></li>
                        <li class="nav-item tab-all"><a href="export_inventaris.php" class="nav-link active show">Export Excel</a></li>
                         <li class="nav-item tab-all"><a href="pdf_inventaris.php" class="nav-link active show">Export PDF</a></li>
                         
                    </ul>
                    <div class="tab-content tab-space">
                       <div class="tab-pane active show" id="tab1">
                         <div class="row">
                              <div class="col-sm-12">
                                <div class="card-box">
                                  <div class="card-head">
                                    <header>Data Masuk Inventari</header> 
                                 </div>
                                      <div class="table-scrollable">
                                        <table class="table table-hover table-checkable order-column full-width" id="example4">
                                            <thead>
                                                <tr>
                                                    <th class="column-title">No </th>
                                                    <th class="column-title">ID Inventaris </th>
                                                    <th class="column-title">Nama Inventaris </th>
                                                    <th class="column-title">Kondisi </th>
                                                    <th class="column-title">Keterangan </th> 
                                                    <th class="column-title">Jumlah </th> 
                                                    <th class="column-title">Nama Jenis </th> 
                                                    <th class="column-title">Tanggal Register </th> 
                                                    <th class="column-title">Nama Ruang </th> 
                                                    <th class="column-title">Kode Inventaris </th> 
                                                    <th class="column-title">Nama Petugas </th> 
                                                    <th class="column-title no-link last"><span class="nobr">Action</span>
                                                    </th>                 
                                                </tr>
                                            </thead>
                                                <?php // Load file koneksi.php
  include "../koneksi.php";
    if (isset($_GET['jurusan'])) {
      $bebas = $_GET['jurusan'];                    
      $query = "SELECT inventaris.*,ruang.nama_ruang,jenis.nama_jenis,petugas.nama_petugas FROM `inventaris` JOIN ruang ON inventaris.id_ruang=ruang.id_ruang JOIN jenis ON jenis.id_jenis=inventaris.id_jenis JOIN petugas ON petugas.id_petugas=inventaris.id_petugas WHERE jenis.nama_jenis = '$bebas' ORDER BY inventaris.id_inventaris DESC"; // Query untuk menampilkan semua data siswa
    } else {
      $query = "SELECT inventaris.*,ruang.nama_ruang,jenis.nama_jenis,petugas.nama_petugas FROM `inventaris` JOIN ruang ON inventaris.id_ruang=ruang.id_ruang JOIN jenis ON jenis.id_jenis=inventaris.id_jenis JOIN petugas ON petugas.id_petugas=inventaris.id_petugas ORDER BY inventaris.id_inventaris DESC";
    }
      $sql = mysqli_query($koneksi, $query); // Eksekusi/Jalankan query dari variabel $query
      $no=1;
     while($data = mysqli_fetch_array($sql)){
?> 
                                            <tbody>
            <tr class="odd gradeX">
                <td><?php echo $no++; ?></td>
                <td><?php echo $data['id_inventaris']; ?></td>
                <td><?php echo $data['nama']; ?></td>
                <td><?php echo $data['kondisi']; ?></td>
                <td><?php echo $data['keterangan']; ?></td>
                <td><?php echo $data['jumlah']; ?></td>
                <td><?php echo $data['nama_jenis']; ?></td>
                <td><?php echo $data['tanggal_register']; ?></td>
                <td><?php echo $data['nama_ruang']; ?></td>
                <td><?php echo $data['kode_inventaris']; ?></td>
                <td><?php echo $data['nama_petugas']; ?></td>
                <td>  
                <a href="edit_inventaris.php?id_inventaris=<?php echo $data['id_inventaris']; ?>" class="btn btn-tbl-edit btn-xs"><i class="fa fa-pencil"></i></a>  
                </td>    
            </tr>
            <?php } ?>
                                           </tbody>
                                           
                                        </table>
                                      </div>
                                    </div>
                                </div>
                              </div>
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
            <!-- end page content -->
    
 <?php
    include'footer.php'; ?>
 