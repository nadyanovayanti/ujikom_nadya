  <?php
 include "header.php";
 ?>

            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li> 
                                <li class="active">Data Pegawai</li>
                            </ol>
                        </div>
                    </div>
                    <ul class="nav nav-pills nav-pills-rose">
                        <li class="nav-item tab-all"><a href="input_pegawai.php" class="nav-link active show">Tambah Pegawai</a></li>
                         
                    </ul>
                    <div class="tab-content tab-space">
                       <div class="tab-pane active show" id="tab1">
                         <div class="row">
                              <div class="col-sm-12">
                                <div class="card-box">
                                  <div class="card-head">
                                    <header>Data Pegawai</header> 
                                 </div>
                                      <div class="table-scrollable">
                                        <table class="table table-hover table-checkable order-column full-width btn-sweetalert" id="example4">
                                            <thead>
                                                <tr>
                                                    <th class="column-title">No </th>
                                                <th class="column-title">ID Pegawai </th>
                                                <th class="column-title">Nama Pegawai </th>
                                                <th class="column-title">NIP </th>
                                                <th class="column-title">Alamat </th> 
                                                <th class="column-title">Username </th> 
                                                <th class="column-title">Password </th> 
                                                <th class="column-title no-link last"><span class="nobr">Action</span>
                                                </th>  
                                                </tr>
                                            </thead>
                                            <tbody> 
<?php
            include "../koneksi.php";
            $no=1;
            $select=mysqli_query($koneksi,"select * from pegawai order by id_pegawai desc");
            while($data=mysqli_fetch_array($select))
            {
            ?>
            <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $data['id_pegawai']; ?></td>
                <td><?php echo $data['nama_pegawai']; ?></td>
                <td><?php echo $data['nip']; ?></td>
                <td><?php echo $data['alamat']; ?></td>
                <td><?php echo $data['username']; ?></td>
                <td><?php echo $data['password']; ?></td>
                <td> 
                <a href="edit_pegawai.php?id_pegawai=<?php echo $data['id_pegawai']; ?>" class="btn btn-tbl-edit btn-xs"><i class="fa fa-pencil"></i></a> 
                <a href="edit_status_pegawai.php?id_pegawai=<?php echo $data['id_pegawai']; ?>" class="btn btn-tbl-edit btn-xs"><?php echo $data['status']; ?></a> 
                </td>     
            </tr>
            <?php
                }
            ?>
                                            </tbody>
                                        </table>
                                      </div>
                                    </div>
                                </div>
                              </div>
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
            <!-- end page content -->
    
 <?php
    include'footer.php'; ?>
 