<?php 
include 'header.php'; 
?> 

<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Generate Laporan</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Generate Laporan</li>
                            </ol>
                        </div>
                    </div>
                   <!-- start widget -->
					<div class="state-overview">
						<div class="row">
					        <div class="col-xl-3 col-md-6 col-12">
					          <div class="info-box bg-blue">
					            <span class="info-box-icon push-bottom"><i class="material-icons">style</i></span>
					            <div class="info-box-content">
					            <?php 
				                  include "../koneksi.php";
				                  $dt = mysqli_query($koneksi, "SELECT sum(jumlah)as jumlah From inventaris where kondisi='Baik'");
								  $d = mysqli_fetch_array($dt);
				                ?>
					              <span class="info-box-text">Barang Masuk</span>
					              <span class="info-box-number"><?php echo $d['jumlah'];?></span>
					               
					                  
					            </div>
					            <!-- /.info-box-content -->
					          </div>
					          <!-- /.info-box -->
					        </div>
					        <!-- /.col -->
					        <div class="col-xl-3 col-md-6 col-12">
					          <div class="info-box bg-orange">
					            <span class="info-box-icon push-bottom"><i class="material-icons">card_travel</i></span>
					            <div class="info-box-content">
					            	 <?php 
				                  include "../koneksi.php";
				                  $dtd = mysqli_query($koneksi, "SELECT sum(jumlah)as jumlah From inventaris where kondisi='Rusak'");
								  $dd = mysqli_fetch_array($dtd);
								  $r= $dd['jumlah'];
								  $y=0;
				                ?>
					              <span class="info-box-text">Barang Rusak</span> 
					              	 <span class='info-box-number'><?php
								  if($r<1){
								  	echo "$y";
								  }else{
								  	echo "$r";
								  }?>
					              </span>
					               
					            </div>
					            <!-- /.info-box-content -->
					          </div>
					          <!-- /.info-box -->
					        </div>
					        <!-- /.col -->
					        <div class="col-xl-3 col-md-6 col-12">
					          <div class="info-box bg-purple">
					            <span class="info-box-icon push-bottom"><i class="material-icons">phone_in_talk</i></span>
					            <div class="info-box-content">
					             <span class="info-box-text">Peminjaman</span>
					            	 <?php 
				                  include "../koneksi.php";
				                  $dtdd = mysqli_query($koneksi, "SELECT sum(status_peminjaman)as status_peminjaman From peminjaman where status_peminjaman='Pinjam'");
				                  $ddd = mysqli_fetch_array($dtdd);
				                  $t=$ddd['status_peminjaman'];
				                  $k=0;
								  ?>
								  <span class='info-box-number'><?php
								  if($t>=1){
								  	echo "$t";
								  }else{
								  	echo "$k";
								  }
				                ?>	         
					              </span>
					            </div>
					            <!-- /.info-box-content -->
					          </div>
					          <!-- /.info-box -->
					        </div>
					        <!-- /.col -->
					        <div class="col-xl-3 col-md-6 col-12">
					          <div class="info-box bg-success">
					            <span class="info-box-icon push-bottom"><i class="material-icons">monetization_on</i></span>
					            <div class="info-box-content"> 
					              <span class="info-box-number"><?php 	 
					                  date_default_timezone_set("asia/Bangkok");				   
									$jam=date("d-m-Y H.i.s"); 
									echo "$jam";
									?></span>
					            <!-- /.info-box-content -->
					          </div>
					          <!-- /.info-box -->
					        </div>
					        <!-- /.col -->
					      </div>
						</div>
					</div>
					<!-- end widget -->
					<div class="row">
                        <div class="col-md-12 col-sm-12">
<!-- MODAL -->
<div id="exportpdf" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap Data Inventaris PDF</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="ex_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="pdf_inventaris.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>
<!-- EXCEL INVENTARIS -->
<div id="exportexcel" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap Data Inventaris Excel</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="excel_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="export_inventaris.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>
<!-- EXCEL ALL DATA PEMINJAMAN -->
<div id="exportexcelall" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap ALL DATA PEMINJAMAN Excel</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="excelall_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="export_all.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>
<!-- EXCEL ALL DATA PEMINJAMAN -->
<div id="pdfall" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap ALL DATA PEMINJAMAN PDF</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="pdfall_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="pdf_all.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>

<div id="exportexcelkem" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap DATA PENGEMBALIAN Excel</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="excelkem_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="export_kembali.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>
<!-- /.MODAL -->
<div id="exportpdfkem" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap DATA PENGEMBALIAN PDF</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="pdfkem_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="pdf_kembali.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>
<!-- /.MODAL -->
<!-- /.MODAL -->
<div id="exportpdfpin" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap DATA PEMIMJAMAN PDF</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="pdfpinjam_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="pdf_pinjam.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>
<!-- /.MODAL -->
<!-- /.MODAL -->
<div id="exportexcelpin" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap DATA PEMIMJAMAN EXCEL</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="excelpinjam_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="export_pinjam.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>
<!-- /.MODAL -->
                         <div class="card card-topline-purple">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header>Export Laporan</header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
                                  <div class="table-wrap">
										<div class="table-responsive">
											<table class="table display product-overview mb-30" id="support_table5">
												<thead>
													<tr>
														<td>Nama</td>
														<td>Export</td>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Inventaris</td>
														<td>
															<div class="col-md-3">
									                        <a data-toggle="modal" data-target="#exportexcel" type="button" class="btn btn-3d btn-success">Excel</a>
									                      </div>||
									                      <div class="col-md-3">
									                        <a data-toggle="modal" data-target="#exportpdf" type="button" class="btn btn-3d btn-success">PDF</a>
									                      </div>
									                  	</td>
													</tr>
													<tr>
														<td>Pengembalian</td>
														<td>
															<div class="col-md-3">
									                        <a data-toggle="modal" data-target="#exportexcelkem" type="button" class="btn btn-3d btn-success">Excel</a>
									                      </div>||
									                      <div class="col-md-3">
									                        <a data-toggle="modal" data-target="#exportpdfkem" type="button" class="btn btn-3d btn-success">PDF</a>
									                      </div>
									                  	</td>
													</tr>
													<tr>
														<td>Peminjaman</td>
														<td>
															<div class="col-md-3">
									                        <a data-toggle="modal" data-target="#exportexcelpin" type="button" class="btn btn-3d btn-success">Excel</a>
									                      </div>||
									                      <div class="col-md-3">
									                        <a data-toggle="modal" data-target="#exportpdfpin" type="button" class="btn btn-3d btn-success">PDF</a>
									                      </div>
									                  	</td>
													</tr>
													<tr>
														<td>All Data Peminjaman</td>
														<td>
															<div class="col-md-3">
									                        <a data-toggle="modal" data-target="#exportexcelall" type="button" class="btn btn-3d btn-success">Excel</a>
									                      </div>||
									                      <div class="col-md-3">
									                        <a data-toggle="modal" data-target="#pdfall" type="button" class="btn btn-3d btn-success">PDF</a>
									                      </div>
									                  	</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
                      
                </div>
            </div>
            <!-- end page content -->
           
        
<?php 
include 'footer.php'; 
?> 