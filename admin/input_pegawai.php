 <?php
 include "header.php";
 ?>
<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                     <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li> 
                                 <li><a class="parent-item" href="dt_msk.php">Data Masuk Pegawai</a>&nbsp;<i class="fa fa-angle-right"></i>
                                <li class="active"> Tambah Data Pegawai</li>
                            </ol>
                        </div>
                    </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="card-box">
                  <div class="card-head">
                    <header>Tambah Data Pegawai</header>                     
                  </div>
                  <form action="simpan_pegawai.php" method="post" enctype="multipart/form-data" name="form1" id="form1" class="card-body row">
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type ="text" name="nama_pegawai" id="nama_pegawai" required="">
                                 <label class = "mdl-textfield__label">Nama Pegawai</label>
                              </div>
                          </div>
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="nip" id="nip" required="">
                                 <label class = "mdl-textfield__label">NIP</label>
                              </div>
                          </div>  
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="alamat" id="alamat" required="">
                                 <label class = "mdl-textfield__label">Alamat</label>
                              </div>
                          </div> 
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="username" id="username" required="">
                                 <label class = "mdl-textfield__label">Username</label>
                              </div>
                          </div> 
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="password" id="password" required="">
                                 <label class = "mdl-textfield__label">Password</label>
                              </div>
                          </div> 
                                                      
                         <div class="col-lg-12 p-t-20 text-center"> 
                          <button type="submit"  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Simpan</button> 

                      <a href="pegawai.php" type="cancel" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</a>
                          </div>
                </form>   

                      
                  </div>
                </div>
              </div>
            </div> 
                </div>
            </div>
            <!-- end page content -->
   <?php
 include "footer.php";
 ?>