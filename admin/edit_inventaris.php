 <?php
 include "header.php";
 include "../koneksi.php";
$id_inventaris=$_GET['id_inventaris'];

$select=mysqli_query($koneksi,"select * from inventaris where id_inventaris='$id_inventaris'");
$data=mysqli_fetch_array($select);
?>
<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                     <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li> 
                                 <li><a class="parent-item" href="dt_msk.php">Data Masuk Inventaris</a>&nbsp;<i class="fa fa-angle-right"></i>
                                <li class="active"> Edit Data Inventaris</li>
                            </ol>
                        </div>
                    </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="card-box">
                  <div class="card-head">
                    <header>Edit Data Inventaris</header>                     
                  </div>
                  <form action="update_inventaris.php?id_inventaris=<?php echo $id_inventaris;?>" method="post" enctype="multipart/form-data" name="form1" id="form1" class="card-body row">
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="nama" id="nama" value="<?php echo $data['nama'];?>">
                                 <label class = "mdl-textfield__label">Nama Inventaris</label>
                              </div>
                          </div>
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="kondisi" id="kondisi" value="<?php echo $data['kondisi'];?>">
                                 <label class = "mdl-textfield__label">Kondisi</label>
                              </div>
                          </div>
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="keterangan" id="keterangan" value="<?php echo $data['keterangan'];?>">
                                 <label class = "mdl-textfield__label">Keterangan</label>
                              </div>
                          </div>
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="jumlah" id="jumlah" value="<?php echo $data['jumlah'];?>">
                                 <label class = "mdl-textfield__label">Jumlah</label>
                              </div>
                          </div>
                          <?php
                            include "../koneksi.php";
                            $result = mysqli_query($koneksi,"select * from jenis order by id_jenis asc ");
                            $jsArray = "var id_jenis = new Array();\n";
                            ?>
                            <div class="col-lg-6 p-t-20"> 
                            <select class="form-control" name="id_jenis" onchange="changeValue(this.value)">
                            <option selected="selected"><?php echo $data['id_jenis'];?>
                            <?php 
                            while($row = mysqli_fetch_array($result)){
                              echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                              $jsArray .= "id_jenis['". $row['id_jenis']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
                            }
                            ?>
                          </option>
                          </select>
                          </div> 
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class="floating-label mdl-textfield__input" type="text" id="date" name="tanggal_register" value="<?php echo $data['tanggal_register'];?>" />
                                 <label class="floating-label mdl-textfield__label">Tanggal</label>
                              </div>
                          </div>

                          <?php
                            include "../koneksi.php";
                            $result = mysqli_query($koneksi,"select * from ruang order by id_ruang asc ");
                            $jsArray = "var id_ruang = new Array();\n";
                            ?>

                            <div class="col-lg-6 p-t-20"> <select class="form-control" name="id_ruang" onchange="changeValue(this.value)">
                            <option selected="selected"><?php echo $data['id_ruang'];?>
                            <?php 
                            while($row = mysqli_fetch_array($result)){
                              echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                              $jsArray .= "id_ruang['". $row['id_ruang']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
                            }
                            ?>
                          </option>
                          </select> 
                          </div>

                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="kode_inventaris" id="kode_inventaris" value="<?php echo $data['nama'];?>">
                                 <label class = "mdl-textfield__label">Kode Inventaris</label>
                              </div>
                          </div>
                          <?php
                          include "../koneksi.php";
                          $result = mysqli_query($koneksi,"select * from petugas order by id_petugas asc ");
                          $jsArray = "var id_petugas = new Array();\n";
                          ?>

                            <div class="col-lg-6 p-t-20"> <select class="form-control" name="id_petugas" onchange="changeValue(this.value)">
                          <option selected="selected"><?php echo $data['id_petugas'];?>
                          <?php 
                          while($row = mysqli_fetch_array($result)){
                            echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                            $jsArray .= "id_petugas['". $row['id_petugas']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
                          }
                          ?>
                        </option>
                        </select> 
                      </div>
                          
                         <div class="col-lg-12 p-t-20 text-center"> 
                          <button name="simpan" type="submit" id="simpan" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Simpan</button> 

                      <a href="dt_msk.php" type="cancel" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</a>
                          </div>
                </form>   

                      
                  </div>
                </div>
              </div>
            </div> 
                </div>
            </div>
            <!-- end page content -->
   <?php
 include "footer.php";
 ?>