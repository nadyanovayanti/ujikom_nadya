  <?php
 include "header.php";
 ?>

            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li> 
                                <li class="active">Data Petugas</li>
                            </ol>
                        </div>
                    </div>
                    <ul class="nav nav-pills nav-pills-rose">
                        <li class="nav-item tab-all"><a href="input_petugas.php" class="nav-link active show">Tambah Petugas</a></li>
                         
                    </ul>
                    <div class="tab-content tab-space">
                       <div class="tab-pane active show" id="tab1">
                         <div class="row">
                              <div class="col-sm-12">
                                <div class="card-box">
                                  <div class="card-head">
                                    <header>Data Petugas</header> 
                                 </div>
                                      <div class="table-scrollable">
                                        <table class="table table-hover table-checkable order-column full-width" id="example4">
                                            <thead>
                                                <tr>
                                                    <th class="column-title">No </th>
                                                    <th class="column-title">ID Petugas </th>
                                                    <th class="column-title">Username </th>
                                                    <th class="column-title">Password </th>
                                                    <th class="column-title">Nama </th>
                                                    <th class="column-title">E-mail </th>
                                                    <th class="column-title">ID Level </th>
                                                    <th class="column-title">Profil </th>
                                                    <th class="column-title">Baned </th>
                                                    <th class="column-title">Login Time </th>
                                                    <th class="column-title no-link last"><span class="nobr">Action</span>
                                                    </th>                       
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                <?php
                                                include "../koneksi.php";
                                                $no=1;
                                                $select=mysqli_query($koneksi,"select * from petugas order by id_petugas desc");
                                                while($data=mysqli_fetch_array($select))
                                                {
                                                ?>
                                                <tr>
                                                    <td><?php echo $no++; ?></td>
                                                    <td><?php echo $data['id_petugas']; ?></td>
                                                    <td><?php echo $data['username']; ?></td>
                                                    <td><?php echo $data['password']; ?></td>
                                                    <td><?php echo $data['nama_petugas']; ?></td>
                                                    <td><?php echo $data['email']; ?></td>
                                                    <td><?php echo $data['id_level']; ?></td>
                                                    <td><img src="<?php echo $data['img']; ?>" alt="..." class="img-circle profile_img" width="20px"></td>
                                                    <td><?php echo $data['baned']; ?></td>
                                                    <td><?php echo $data['logintime']; ?></td>
                                                    <td>
                                                    <a href="edit_petugas.php?id_petugas=<?php echo $data['id_petugas']; ?>" class="btn btn-tbl-edit btn-xs"><i class="fa fa-pencil"></i></a>
                                                    <a href="hapus_petugas.php?id_petugas=<?php echo $data['id_petugas']; ?>" class="btn btn-tbl-delete btn-xs"><i class="fa fa-trash-o "></i></a></td>    
                                                </tr>
                                                <?php
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                      </div>
                                    </div>
                                </div>
                              </div>
                          </div>
                        </div>
                    </div>
                </div> 
            </div>
            <!-- end page content -->
    
 <?php
    include'footer.php'; ?>
 