 <?php
 include "header.php";
 ?>
<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                     <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li> 
                                 <li><a class="parent-item" href="dt_msk.php">Data Masuk Inventaris</a>&nbsp;<i class="fa fa-angle-right"></i>
                                <li class="active"> Tambah Data Inventaris</li>
                            </ol>
                        </div>
                    </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="card-box">
                  <div class="card-head">
                    <header>Tambah Data Inventaris</header>                     
                  </div>
                  <form action="simpan_inventaris.php" method="post" enctype="multipart/form-data" name="form1" id="form1" class="card-body row">
                              
                           
                            <div class="col-lg-6 p-t-20">
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width"> <?php
                            include "../koneksi.php";
                            $use=$_SESSION['username'];
                            $result = mysqli_query($koneksi,"select * from petugas where username='$use'");
                            $jsArray = "var id_petugas = new Array();\n";
                            ?>

                            <input class = "mdl-textfield__input" name="id_petugas" value="<?php while($row =mysqli_fetch_array($result)){echo "$row[0].$row[3]";
                              $jsArray .= "id_petugas['". $row['id_petugas']. "'] = {satu:'" . addslashes($row['id_petugas']) . "'};\n";}
                            ?>" readonly>

                          <label class = "mdl-textfield__label" >Nama Petugas</label>
                          </input> 
                          </div>
                        </div>
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type ="text" name="nama" id="nama" required="">
                                 <label class = "mdl-textfield__label">Nama Inventaris</label>
                              </div>
                          </div> 
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="keterangan" id="keterangan" required="">
                                 <label class = "mdl-textfield__label">Keterangan</label>
                              </div>
                          </div>
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="jumlah" id="jumlah" required="">
                                 <label class = "mdl-textfield__label">Jumlah</label>
                              </div>
                          </div>
                          <?php
                            include "../koneksi.php";
                            $result = mysqli_query($koneksi,"select * from jenis order by id_jenis asc ");
                            $jsArray = "var id_jenis = new Array();\n";
                            ?>
                            <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <select class = "mdl-textfield__input" name="id_jenis" onchange="changeValue(this.value)" required>
                            <option selected="selected" required>
                            <?php 
                            while($row = mysqli_fetch_array($result)){
                              echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                              $jsArray .= "id_jenis['". $row['id_jenis']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
                            }
                            ?>
                          </option>
                          </select>                          
                              <label class = "mdl-textfield__label" >Pilih Jenis</label>
                          </div> 
                          </div> 
                          <?php
                            include "../koneksi.php";
                            $result = mysqli_query($koneksi,"select * from ruang order by id_ruang asc ");
                            $jsArray = "var id_ruang = new Array();\n";
                            ?>
                            <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width"> 
                            <select class = "mdl-textfield__input" name="id_ruang" onchange="changeValue(this.value)" required>
                            <option selected="selected" required>
                            <?php 
                            while($row = mysqli_fetch_array($result)){
                              echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                              $jsArray .= "id_ruang['". $row['id_ruang']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
                            }
                            ?>
                          </option>
                          </select> 
                          <label class = "mdl-textfield__label" >Pilih Ruang</label>
                          </div> 
                          </div> 
                      <?php
                       $koneksi = mysqli_connect('localhost','root','','inventaris_smk');

                      $cari_km=mysqli_query($koneksi,"select max(kode_inventaris) as kode from inventaris");
                      //mencari kode yang paling besar atau kode yang barang masuk
                        $tm_cari=mysqli_fetch_array($cari_km);
                        $kode=substr($tm_cari['kode'],1,4);
                      //mengambil string mulai dari karakter pertama 'A' dan mengambil karakter setelah nya
                        $tambah=$kode+1;
                      //kode yang sudah dipecah di tambah 1
                          if($tambah<10){ //jika kode lebih kecil dari 10(0,8,7,6 dst) maka
                              $kode_inventaris="B000".$tambah;  
                          } else{
                              $kode_inventaris="B00".$tambah;
                          }

                          ?>

                        <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width"> 
                            <input type="text" name="kode_inventaris" value="<?php echo $kode_inventaris; ?>" class = "mdl-textfield__input" readonly=""> 
                          <label class = "mdl-textfield__label" >Kode</label>
                        </div>
                        </div>

                         <div class="col-lg-12 p-t-20 text-center"> 
                          <button name="simpan" type="submit" id="simpan" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Simpan</button> 

                      <a href="dt_msk.php" type="cancel" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</a>
                          </div>
                </form>   

                      
                  </div>
                </div>
              </div>
            </div> 
                </div>
            </div>
            <!-- end page content -->
   <?php
 include "footer.php";
 ?>