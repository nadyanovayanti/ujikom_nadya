
 <?php
 include "header.php";
 ?>
<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                     <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li> 
                                 <li><a class="parent-item" href="ruang.php">Data Masuk Ruang</a>&nbsp;<i class="fa fa-angle-right"></i>
                                <li class="active"> Tambah Data Ruang</li>
                            </ol>
                        </div>
                    </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="card-box">
                  <div class="card-head">
                    <header>Tambah Data Ruang</header>                     
                  </div>
                  <form action="simpan_ruang.php" method="post" enctype="multipart/form-data" name="form1" id="form1" class="card-body row">
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type ="text" name="nama_ruang" id="nama_ruang" required="">
                                 <label class = "mdl-textfield__label">Nama Ruang</label>
                              </div>
                          </div>
                          <?php
                       $koneksi = mysqli_connect('localhost','root','','inventaris_smk');

                      $cari_km=mysqli_query($koneksi,"select max(kode_ruang) as kode from ruang");
                      //mencari kode yang paling besar atau kode yang barang masuk
                        $tm_cari=mysqli_fetch_array($cari_km);
                        $kode=substr($tm_cari['kode'],1,4);
                      //mengambil string mulai dari karakter pertama 'A' dan mengambil karakter setelah nya
                        $tambah=$kode+1;
                      //kode yang sudah dipecah di tambah 1
                          if($tambah<10){ //jika kode lebih kecil dari 10(0,8,7,6 dst) maka
                              $kode_ruang="R000".$tambah;  
                          } else{
                              $kode_ruang="R00".$tambah;
                          }

                          ?> 
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="kode_ruang" id="kode_ruang" value="<?php echo $kode_ruang; ?>" required="">
                                 <label class = "mdl-textfield__label">Kode Ruangan</label>
                              </div>
                          </div>  
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="keterangan" id="keterangan" required="">
                                 <label class = "mdl-textfield__label">Keterangan</label>
                              </div>
                          </div> 
                                                      
                         <div class="col-lg-12 p-t-20 text-center"> 
                          <button type="submit"  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Simpan</button> 

                      <a href="pegawai.php" type="cancel" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</a>
                          </div>
                </form>   

                      
                  </div>
                </div>
              </div>
            </div> 
                </div>
            </div>
            <!-- end page content -->
   <?php
 include "footer.php";
 ?>