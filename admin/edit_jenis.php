<?php
 include "header.php";
include "../koneksi.php";
$id_jenis=$_GET['id_jenis'];

$select=mysqli_query($koneksi,"select * from jenis where id_jenis='$id_jenis'");
$data=mysqli_fetch_array($select);
?>
  <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                     <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li> 
                                 <li><a class="parent-item" href="jenis.php">Data Jenis Inventaris</a>&nbsp;<i class="fa fa-angle-right"></i>
                                <li class="active"> Edit Jenis Inventaris</li>
                            </ol>
                        </div>
                    </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="card-box">
                  <div class="card-head">
                    <header>Edit Jenis Inventaris</header>                     
                  </div>
                  <form action="update_jenis.php?id_jenis=<?php echo $id_jenis;?>" method="post" enctype="multipart/form-data" name="form1" id="form1" class="card-body row">
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="nama_jenis" id="nama_jenis" value="<?php echo $data['nama_jenis'];?>">
                                 <label class = "mdl-textfield__label">Nama Jenis</label>
                              </div>
                          </div>
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="kode_jenis" id="kode_jenis" value="<?php echo $data['kode_jenis'];?>">
                                 <label class = "mdl-textfield__label">Kode Jenis</label>
                              </div>
                          </div>
                          <div class="col-lg-6 p-t-20"> 
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <input class = "mdl-textfield__input" type = "text" name="keterangan" id="keterangan" value="<?php echo $data['keterangan'];?>">
                                 <label class = "mdl-textfield__label">Keterangan</label>
                              </div>
                          </div>
                                                   
                         <div class="col-lg-12 p-t-20 text-center"> 
                          <button name="simpan" type="submit" id="simpan" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Simpan</button> 

                      <a href="jenis.php" type="cancel" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</a>
                          </div>
                </form>   

                      
                  </div>
                </div>
              </div>
            </div> 
                </div>
            </div>
            <!-- end page content -->
   <?php
 include "footer.php";
 ?>