<?php  
session_start(); 
include "../koneksi.php";
 
///////////////////////////////////////////////////////////////////////
if (isset($_POST['user_log'])) {
    $username = $_POST['username']; 
    $pass = $_POST['password']; 

    $sql ="SELECT * from pegawai where username='$username' and password='$pass'";
    $query = mysqli_query($koneksi,$sql);
    $cek=mysqli_num_rows($query);

    if($cek>0){
    	session_start(); 
	    $data=mysqli_fetch_assoc($query); 
    	$_SESSION['username']=$username;
    	$_SESSION['id_pegawai']=$cek['id_pegawai']; 
    	header("location:../peminjam/index.php");
    }else{
    	header("<script>alert('username atau password salah');location:'log_user.php'</script>/n");
    }
}?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Responsive Admin Template" />
    <meta name="author" content="SmartUniversity" />
    <title>Spice Hotel | Bootstrap 4 Admin Dashboard Template + UI Kit</title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="../assets/plugins/iconic/css/material-design-iconic-font.min.css">
    <!-- bootstrap -->
	<link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- style -->
    <link rel="stylesheet" href="../assets/css/pages/extra_pages.css">
	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/favicon.ico" /> 
</head>
<body>
    <div class="limiter">
		<div class="container-login100 page-background">
			<div class="wrap-login100">
				<form class="login100-form validate-form" action="" method="post">
					<span class="login100-form-logo">
						<i class="zmdi zmdi-flower"></i>
					</span>
					<span class="login100-form-title p-b-34 p-t-27">
						Log in User
					</span>
					<div class="wrap-input100 validate-input" data-validate = "Enter ID">
						<input class="input100" type="text"  type="text" name="username" id="username" placeholder="Masukan Nama">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" id="password" name="password" placeholder="Password">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>
					 
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" name="user_log" type="submit">
							Masuk
						</button>
					</div>
					<div class="text-center p-t-90">
						<a class="txt1" href="index.php">
							Kembali Login Admin!!!
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
    <!-- start js include path -->
    <script src="../assets/plugins/jquery/jquery.min.js" ></script>
    <!-- bootstrap -->
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <script src="../assets/js/pages/extra_pages/login.js" ></script>
    <!-- end js include path -->
</body>
</html>