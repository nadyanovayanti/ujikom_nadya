<?php 
include 'header.php'; 
?> 

<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Dashboard</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Dashboard</li>
                            </ol>
                        </div>
                    </div>
                   <!-- start widget -->
					<div class="state-overview">
						<div class="row">
					        <div class="col-xl-3 col-md-6 col-12">
					          <div class="info-box bg-blue">
					            <span class="info-box-icon push-bottom"><i class="material-icons">style</i></span>
					            <div class="info-box-content">
					            <?php 
				                  include "../koneksi.php";
				                  $dt = mysqli_query($koneksi, "SELECT sum(jumlah)as jumlah From inventaris where kondisi='Baik'");
								  $d = mysqli_fetch_array($dt);
				                ?>
					              <span class="info-box-text">Barang Masuk</span>
					              <span class="info-box-number"><?php echo $d['jumlah'];?></span>
					               
					                  
					            </div>
					            <!-- /.info-box-content -->
					          </div>
					          <!-- /.info-box -->
					        </div>
					        <!-- /.col -->
					        <div class="col-xl-3 col-md-6 col-12">
					          <div class="info-box bg-orange">
					            <span class="info-box-icon push-bottom"><i class="material-icons">card_travel</i></span>
					            <div class="info-box-content">
					            	 <?php 
				                  include "../koneksi.php";
				                  $dtd = mysqli_query($koneksi, "SELECT sum(jumlah)as jumlah From inventaris where kondisi='Rusak'");
								  $dd = mysqli_fetch_array($dtd);
								  $r= $dd['jumlah'];
								  $y=0;
				                ?>
					              <span class="info-box-text">Barang Rusak</span> 
					              	 <span class='info-box-number'><?php
								  if($r<1){
								  	echo "$y";
								  }else{
								  	echo "$r";
								  }?>
					              </span>
					               
					            </div>
					            <!-- /.info-box-content -->
					          </div>
					          <!-- /.info-box -->
					        </div>
					        <!-- /.col -->
					        <div class="col-xl-3 col-md-6 col-12">
					          <div class="info-box bg-purple">
					            <span class="info-box-icon push-bottom"><i class="material-icons">phone_in_talk</i></span>
					            <div class="info-box-content">
					             <span class="info-box-text">Peminjaman</span>
					            	 <?php 
				                  include "../koneksi.php";
				                  $dtdd = mysqli_query($koneksi, "SELECT sum(status_peminjaman)as status_peminjaman From detail_pinjam where status_peminjaman='Pinjam'");
				                  $ddd = mysqli_fetch_array($dtdd);
				                  $t=$ddd['status_peminjaman'];
				                  $k=0;
								  ?>
								  <span class='info-box-number'><?php
								  if($t>=1){
								  	echo "$t";
								  }else{
								  	echo "$k";
								  }
				                ?>	         
					              </span>
					            </div>
					            <!-- /.info-box-content -->
					          </div>
					          <!-- /.info-box -->
					        </div>
					        <!-- /.col -->
					        <div class="col-xl-3 col-md-6 col-12">
					          <div class="info-box bg-success">
					            <span class="info-box-icon push-bottom"><i class="material-icons">monetization_on</i></span>
					            <div class="info-box-content"> 
					              <span class="info-box-number"><?php 	 
					                  date_default_timezone_set("asia/Bangkok");				   
									$jam=date("d-m-Y H.i.s"); 
									echo "$jam";
									?></span>
					            <!-- /.info-box-content -->
					          </div>
					          <!-- /.info-box -->
					        </div>
					        <!-- /.col -->
					      </div>
						</div>
					</div>
					<!-- end widget --> 
                                   
                     <div class="row">
                        <div class="col-md-12 col-sm-12">
                         <div class="card card-topline-purple">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header>Daftar Barang Inventaris</header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
                                  <div class="table-wrap">
										<div class="table-responsive">
											<table class="table display product-overview mb-30" id="support_table5">
												<thead>
													<tr>
													<th class="column-title">No </th> 
                                                    <th class="column-title">Nama Inventaris </th>  
                                                    <th class="column-title">Jumlah </th> 
                                                    <th class="column-title">Jenis </th>  
                                                    <th class="column-title">Ruang </th> 
                                                    <th class="column-title">Kode Inventaris </th> 
													</tr>
												</thead>
												<tbody>
													<?php
            include "../koneksi.php";
            $no=1;
            $select=mysqli_query($koneksi,"SELECT * from inventaris INNER JOIN jenis ON inventaris.id_jenis=jenis.id_jenis INNER JOIN ruang ON inventaris.id_ruang=ruang.id_ruang order by id_inventaris desc");
            while($data=mysqli_fetch_array($select))
            {
            ?>
            <tr class="odd gradeX">
                <td><?php echo $no++; ?></td> 
                <td><?php echo $data['nama']; ?></td>  
                <td><?php echo $data['jumlah']; ?></td>
                <td><?php echo $data['nama_jenis']; ?></td> 
                <td><?php echo $data['nama_ruang']; ?></td>
                <td><?php echo $data['kode_inventaris']; ?></td>     
            </tr>
            <?php
                }
            ?>
												</tbody>
											</table>
										</div>
									</div>	
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <!-- end Payment Details -->
                </div>
            </div>
            <!-- end page content -->
        </div>
        
<?php 
include 'footer.php'; 
?> 