 </div>
        <!-- end page container -->
        <!-- start footer -->
       <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy; Aplikasi Inventaris By
            <a href="mailto:nadyaanovaa6@gmail.com" target="_top" class="makerCss">NadyaNovayanti</a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets/plugins/jquery/jquery.min.js" ></script>
    <script src="../assets/plugins/popper/popper.min.js" ></script>
    <script src="../assets/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- bootstrap -->
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <script src="../assets/plugins/sparkline/jquery.sparkline.min.js" ></script>
	<script src="../assets/js/pages/sparkline/sparkline-data.js" ></script>
    <!-- Common js-->
	<script src="../assets/js/app.js" ></script>
    <script src="../assets/js/layout.js" ></script>
    <script src="../assets/js/theme-color.js" ></script>
    <!-- material -->
    <script src="../assets/plugins/material/material.min.js"></script>
    <script src="../assets/js/pages/material_select/getmdl-select.js" ></script>
    <script  src="../assets/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
    <script  src="../assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
    <script  src="../assets/plugins/material-datetimepicker/datetimepicker.js"></script>
    <!-- animation -->
    <script src="../assets/js/pages/ui/animations.js" ></script>
    <!-- data tables -->
    <script src="../assets/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="../assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets/js/pages/table/table_data.js" ></script> 
    <!-- morris chart -->
    <script src="../assets/plugins/morris/morris.min.js" ></script>
    <script src="../assets/plugins/morris/raphael-min.js" ></script>
    <script src="../assets/js/pages/chart/morris/morris_home_data.js" ></script>
    <!-- dropzone -->
    <script src="../assets/plugins/dropzone/dropzone.js" ></script>
    <script src="../assets/plugins/dropzone/dropzone-call.js" ></script>
    <!-- end js include path -->

</html>