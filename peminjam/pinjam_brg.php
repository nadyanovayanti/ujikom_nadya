<?php 
include 'header.php'; 
$use=$_SESSION['username'];
$result = mysqli_query($koneksi,"select nama_pegawai,id_pegawai from pegawai where username='$use'");
$row =mysqli_fetch_array($result);
$id_pegawai=$row['nama_pegawai'];
$id=$row['id_pegawai'];
?> 

<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Dashboard</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Dashboard</li>
                            </ol>
                        </div>
                    </div>
                   <!-- start widget -->
          <div class="state-overview">
            <div class="row">
                  <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-blue">
                      <span class="info-box-icon push-bottom"><i class="material-icons">style</i></span>
                      <div class="info-box-content">
                      <?php 
                          include "../koneksi.php";
                          $dt = mysqli_query($koneksi, "SELECT sum(jumlah)as jumlah From inventaris where kondisi='Baik'");
                  $d = mysqli_fetch_array($dt);
                        ?>
                        <span class="info-box-text">Barang Masuk</span>
                        <span class="info-box-number"><?php echo $d['jumlah'];?></span>
                         
                            
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-orange">
                      <span class="info-box-icon push-bottom"><i class="material-icons">card_travel</i></span>
                      <div class="info-box-content">
                         <?php 
                          include "../koneksi.php";
                          $dtd = mysqli_query($koneksi, "SELECT sum(jumlah)as jumlah From inventaris where kondisi='Rusak'");
                  $dd = mysqli_fetch_array($dtd);
                  $r= $dd['jumlah'];
                  $y=0;
                        ?>
                        <span class="info-box-text">Barang Rusak</span> 
                           <span class='info-box-number'><?php
                  if($r<1){
                    echo "$y";
                  }else{
                    echo "$r";
                  }?>
                        </span>
                         
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-purple">
                      <span class="info-box-icon push-bottom"><i class="material-icons">phone_in_talk</i></span>
                      <div class="info-box-content">
                       <span class="info-box-text">Peminjaman</span>
                         <?php 
                          include "../koneksi.php";
                          $dtdd = mysqli_query($koneksi, "SELECT sum(status_peminjaman)as status_peminjaman,id_pegawai From peminjaman where status_peminjaman='Pinjam' and id_pegawai='$id' ");
                          $ddd = mysqli_fetch_array($dtdd);
                          $t=$ddd['status_peminjaman'];
                          $k=0;
                  ?>
                  <span class='info-box-number'><?php
                  if($t>=1){
                    echo "$t";
                  }else{
                    echo "$k";
                  }
                        ?>           
                        </span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-success">
                      <span class="info-box-icon push-bottom"><i class="material-icons">monetization_on</i></span>
                      <div class="info-box-content"> 
                        <span class="info-box-number"><?php    
                            date_default_timezone_set("asia/Bangkok");           
                  $jam=date("d-m-Y H.i.s"); 
                  echo "$jam";
                  ?></span>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                </div>
            </div>
          </div>
          <!-- end widget --> 
                      <div class="row">
              <div class="col-sm-12">
                <div class="card-box">
                  <div class="card-head">
                    <header>Tambah Transaksi Baru</header>                     
                  </div>
                  <form action="pro_pinjam.php" method="post" enctype="multipart/form-data" name="form1" id="form1" class="card-body row">
                          <?php
                            include "../koneksi.php";
                            $id_peminjaman=$_GET['id_peminjaman'];
                            $result =mysqli_fetch_array(mysqli_query($koneksi,"SELECT * FROM peminjaman where id_peminjaman='$id_peminjaman' ")); 
                            ?>

                            <div class="col-lg-6 p-t-20">
                            <label>ID//Nama</label> 
                            <input class="form-control" name="id_peminjaman" value="<?php echo $result['id_peminjaman']; ?>" readonly="">
                          </input> 
                          </div>
                          <div class="col-lg-6 p-t-20"> </div>
                            <?php
                            include "../koneksi.php";
                            $result = mysqli_query($koneksi,"select * from inventaris order by id_inventaris desc ");
                            $jsArray = "var id_inventaris = new Array();\n";
                            ?>
                            <div class="col-lg-6 p-t-20"> 
                            <label>Cari Barang</label>
                            <select class="form-control" name="id_inventaris" onchange="changeValue(this.value)" required="">
                            <option selected="selected" required="">
                            <?php 
                            while($row = mysqli_fetch_array($result)){
                              echo "<option value='$row[id_inventaris]'>$row[nama](stock: $row[jumlah])</option>";
                              $jsArray .= "id_inventaris['". $row['id_inventaris']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
 
                            }
                            ?>
                          </option>
                          </select> 
                        </div>
                         <div class="col-lg-6 p-t-20"> 
                          <label>Jumlah Barang :</label>
                           <input type="number" name="jumlah1" class="form-control"  placeholder="Masukan Jumlah Barang..." required="">
                        </div> 
                         <div class="col-lg-12 p-t-20 text-center"> 
                          <button name="simpan" type="submit" id="simpan" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Pilih</button> 
                            <a href="index.php" id="simpan" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-blue">Selesai</a>  </div>
                </form>                         
                  </div>
                </div>

                <div class="col-md-12 col-lg-6">
                                    <div class="card card-topline-purple">
                                        <div class="card-head">
                                            <header>Data Transaksi</header>
                                            <div class="tools">
                                                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                                <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                                <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                            </div>
                                        </div>
                                        <div class="card-body ">
                                        <div class="table-responsive">
                                          <table class="table display product-overview mb-30" id="support_table5">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>ID Barang</th> 
                                                        <th>Jumlah</th> 
                                                        <th>ID Peminjaman</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody> 
                                                  <?php
                          include "../koneksi.php"; 
                          $no=1; 
                           $query_mysqli = mysqli_query ($koneksi,"SELECT * from detail_pinjam where id_peminjaman='$_GET[id_peminjaman]' ORDER BY id_detail_pinjam DESC");
                          while($data = mysqli_fetch_array($query_mysqli)){
                        ?>
                      <tr>
                        <td><?php echo $no++ ?></td> 
                        <td><?php echo $data['id_inventaris'] ?></td>
                        <td align="center"><?php echo $data['jumlah1'] ?></td>    
                        <td align="center"><?php echo $data['id_peminjaman'] ?></td>  
                        <td align="center"><?php echo $data['status_peminjaman'] ?></td>    
                      </tr>
                      <?php } ?>
                                                </tbody>
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <div class="col-md-12 col-lg-6">
                         <div class="card card-topline-purple">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header>Daftar Barang Inventaris</header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                      <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                      <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
                                  <div class="table-wrap">
                    <div class="table-responsive">
                      <table class="table display product-overview mb-30" id="support_table5">
                        <thead>
                          <tr>
                          <th class="column-title">No </th> 
                                                    <th class="column-title">Nama Inventaris </th>  
                                                    <th class="column-title">Jumlah </th> 
                                                    <th class="column-title">Jenis </th>  
                                                    <th class="column-title">Ruang </th> 
                                                    <th class="column-title">Kode Inventaris </th> 
                          </tr>
                        </thead>
                        <tbody>
                          <?php
            include "../koneksi.php";
            $no=1;
            $select=mysqli_query($koneksi,"SELECT * from inventaris INNER JOIN jenis ON inventaris.id_jenis=jenis.id_jenis INNER JOIN ruang ON inventaris.id_ruang=ruang.id_ruang order by id_inventaris desc");
            while($data=mysqli_fetch_array($select))
            {
            ?>
            <tr class="odd gradeX">
                <td><?php echo $no++; ?></td> 
                <td><?php echo $data['nama']; ?></td>  
                <td><?php echo $data['jumlah']; ?></td>
                <td><?php echo $data['nama_jenis']; ?></td> 
                <td><?php echo $data['nama_ruang']; ?></td>
                <td><?php echo $data['kode_inventaris']; ?></td>     
            </tr>
            <?php
                }
            ?>
                        </tbody>
                      </table>
                    </div>
                  </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <!-- end Payment Details -->
              </div>
            </div> 
                     
                    </div>
                </div> 
            </div>
            <!-- end page content -->
    
 <?php
    include'footer.php'; ?>
 