<?php 
include 'header.php'; 
$use=$_SESSION['username'];
$result = mysqli_query($koneksi,"select nama_pegawai,id_pegawai from pegawai where username='$use'");
$row =mysqli_fetch_array($result);
$id_pegawai=$row['nama_pegawai'];
$id=$row['id_pegawai'];
?> 

<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Dashboard</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Dashboard</li>
                            </ol>
                        </div>
                    </div>
                   <!-- start widget -->
          <div class="state-overview">
            <div class="row">
                  <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-blue">
                      <span class="info-box-icon push-bottom"><i class="material-icons">style</i></span>
                      <div class="info-box-content">
                      <?php 
                          include "../koneksi.php";
                          $dt = mysqli_query($koneksi, "SELECT sum(jumlah)as jumlah From inventaris where kondisi='Baik'");
                  $d = mysqli_fetch_array($dt);
                        ?>
                        <span class="info-box-text">Barang Masuk</span>
                        <span class="info-box-number"><?php echo $d['jumlah'];?></span>
                         
                            
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-orange">
                      <span class="info-box-icon push-bottom"><i class="material-icons">card_travel</i></span>
                      <div class="info-box-content">
                         <?php 
                          include "../koneksi.php";
                          $dtd = mysqli_query($koneksi, "SELECT sum(jumlah)as jumlah From inventaris where kondisi='Rusak'");
                  $dd = mysqli_fetch_array($dtd);
                  $r= $dd['jumlah'];
                  $y=0;
                        ?>
                        <span class="info-box-text">Barang Rusak</span> 
                           <span class='info-box-number'><?php
                  if($r<1){
                    echo "$y";
                  }else{
                    echo "$r";
                  }?>
                        </span>
                         
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-purple">
                      <span class="info-box-icon push-bottom"><i class="material-icons">phone_in_talk</i></span>
                      <div class="info-box-content">
                       <span class="info-box-text">Peminjaman</span>
                         <?php 
                          include "../koneksi.php";
                          $dtdd = mysqli_query($koneksi, "SELECT sum(status_peminjaman)as status_peminjaman,id_pegawai From peminjaman where status_peminjaman='Pinjam' and id_pegawai='$id' ");
                          $ddd = mysqli_fetch_array($dtdd);
                          $t=$ddd['status_peminjaman'];
                          $k=0;
                  ?>
                  <span class='info-box-number'><?php
                  if($t>=1){
                    echo "$t";
                  }else{
                    echo "$k";
                  }
                        ?>           
                        </span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-success">
                      <span class="info-box-icon push-bottom"><i class="material-icons">monetization_on</i></span>
                      <div class="info-box-content"> 
                        <span class="info-box-number"><?php    
                            date_default_timezone_set("asia/Bangkok");           
                  $jam=date("d-m-Y H.i.s"); 
                  echo "$jam";
                  ?></span>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                </div>
            </div>
          </div>
          <!-- end widget --> 
                <div class="col-md-12">
                                    <div class="card card-topline-purple">
                                        <div class="card-head">
                                            <header>Data Transaksi</header>
                                            <div class="tools">
                                                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                                <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                                <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                            </div>
                                        </div>
                                        <div class="card-body ">
                                        <div class="table-responsive">
                                            <table id="example2" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Peminjam</th>
                                                        <th>Tanggal Pinjam</th>
                                                        <th>Tanggal Kembali</th>
                                                        <th>Status</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody> 
                                                  <?php
                          include "../koneksi.php";
                          $no=1;                            
                          $qw = mysqli_query ($koneksi,"SELECT * from peminjaman INNER JOIN pegawai ON peminjaman.id_pegawai=pegawai.id_pegawai where nama_pegawai='$id_pegawai' ORDER BY id_peminjaman DESC");
                          while($data = mysqli_fetch_array($qw)){
                        ?>
                      <tr>
                        <td><?php echo $no++ ?></td> 
                        <td><?php echo $data['nama_pegawai'] ?></td>
                        <td align="center"><?php echo $data['tanggal_pinjam'] ?></td>
                        <td><?php echo $data['tanggal_kembali'] ?></td>
                        <td align="center"><?php echo $data['status_peminjaman'] ?></td>  
                         <td>
                      <div class="col-md-6">
                        <a href="detail_pembelian.php?id_peminjaman=<?php echo $data['id_peminjaman'];?>" type="button" class="btn btn-3d btn-success">View</a>
                      </div>
                    </td>
                    </tr>
                      <?php }?>
                                                </tbody>
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
              </div>
            </div> 
                     
                    </div>
                </div> 
            </div>
            <!-- end page content -->
    
 <?php
    include'footer.php'; ?>
 